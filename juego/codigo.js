const obtenerNumAleatorio = (min, max) => {
    return Math.floor((Math.random() * (max - min )) + min);
}

var app = new Vue({
    el: '#app',
    data: {
        ganadas: 0,
        empates: 0,
        perdidas: 0,
        resultado: null,
        opcionMaquina: null,
        opcionUsuario: null
    },
    methods: {
        usuarioGano: function(){
            this.ganadas ++;
            this.resultado = 'Ganaste'
        },
        usuarioPerdio: function(){
            this.perdidas ++;
            this.resultado = 'Perdiste'
        },
        usuarioEmpato: function(){
            this.empates ++;
            this.resultado = 'Empate'
        },
        iniciarRonda: function(opcion){
            let opcionesExistentes = ['papel', 'piedra', 'tijera', 'papel'];

            const aleatorio = obtenerNumAleatorio( 0, 3 );

            const opcionMaquina = opcionesExistentes[aleatorio];

            this.opcionUsuario = opcion
            this.opcionMaquina = opcionMaquina

            if( this.opcionUsuario === this.opcionMaquina ){
                return this.usuarioEmpato()
            }

            const posicionOpcionUsuario = opcionesExistentes.indexOf(this.opcionUsuario);
        
            //Si la maquina eligio el elemento siguiente al elemento del usuario, gano
            if( opcionesExistentes[posicionOpcionUsuario + 1] === opcionMaquina ) {
                return this.usuarioGano()
            }else {
                return this.usuarioPerdio()
            }  
        }
    }
})